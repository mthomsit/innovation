/*** AJAX & JS Configuration parameters for territories ***/

/* Live search */
	
	var livesearchon = ""; //Enable or disable live search. "true" for on. blank or any other value for off.
	var minchars = 2; //Min number of characters input before suggestions are triggered
	
	// Change to location of custom live search xml file
	var suggestionsfile = "/en_GX/webadmin/ajax/livesearch/livesearch.xml";

/* Send & share links */

	// NB! This is required for all cc-ll.js files regardless
	var arr_ss = new Array();

	// EMAIL
	arr_ss [0] = new Array()
	arr_ss [0][0] = "/en_GX/webadmin/assets/image/share_email.gif";
	arr_ss [0][1] = "javascript:emailpage();";
	arr_ss [0][2] = ""
	arr_ss [0][3] = "Email this page";
	arr_ss [0][4] = "";
	arr_ss [0][5] = "";	
	
	// Twitter
	arr_ss [1] = new Array()
	arr_ss [1][0] = "/en_GX/webadmin/assets/image/share_twitter.gif";
	arr_ss [1][1] = "http://twitter.com/home?status=" + escape(window.location);
	arr_ss [1][2] = "?WT.mc_id=MRK091111SM5";
	arr_ss [1][3] = "Twitter";
	arr_ss [1][4] = "wrs_trackclick(\"DCS.dcsuri=http://www.twitter.com_sharebookmark\",\"WT.ti=Offsite: http://www.twitter.com_sharebookmark\",\"WT.dl=24\",\"WT.z_smch=Twitter\",\"WT.z_smev=Share\",\"WT.z_smShare=1\")";
	arr_ss [1][5] = "_new";

	// Facebook
	arr_ss [2] = new Array()
	arr_ss [2][0] = "/en_GX/webadmin/assets/image/share_facebook.gif";
	arr_ss [2][1] = "http://www.facebook.com/share.php?u=" + escape(window.location);
	arr_ss [2][2] = "?WT.mc_id=MRK091111SM5";
	arr_ss [2][3] = "Facebook";
	arr_ss [2][4] = "wrs_trackclick(\"DCS.dcsuri=http://www.facebook.com_sharebookmark\",\"WT.ti=Offsite: http://www.facebook.com_sharebookmark\",\"WT.dl=24\",\"WT.z_smch=Facebook\",\"WT.z_smev=Share\",\"WT.z_smShare=1\")";
	arr_ss [2][5] = "_new";
	
	// Linkedin
	arr_ss [3] = new Array()
	arr_ss [3][0] = "/en_GX/webadmin/assets/image/share_linked.gif";
	arr_ss [3][1] = "http://www.linkedin.com/shareArticle?mini=true&url=" + escape(window.location);
	arr_ss [3][2] = "?WT.mc_id=MRK091111SM5";
	arr_ss [3][3] = "Linkedin";
	arr_ss [3][4] = "wrs_trackclick(\"DCS.dcsuri=http://www.linkedin.com_sharebookmark\",\"WT.ti=Offsite: http://www.linkedin.com_sharebookmark\",\"WT.dl=24\",\"WT.z_smch=Linked_In\",\"WT.z_smev=Share\",\"WT.z_smShare=1\")";
	arr_ss [3][5] = "_new";

	// Google + here in future
	
	// Digg
	arr_ss [4] = new Array()
	arr_ss [4][0] = "/en_GX/webadmin/assets/image/share_digg.gif";
	arr_ss [4][1] = "http://digg.com/submit?phase=2&url=" + escape(window.location);
	arr_ss [4][2] = "?WT.mc_id=MRK091111SM5";
	arr_ss [4][3] = "digg";
	arr_ss [4][4] = "wrs_trackclick(\"DCS.dcsuri=http://www.digg.com_sharebookmark\",\"WT.ti=Offsite: http://www.digg.com_sharebookmark\",\"WT.dl=24\",\"WT.z_smch=Digg\",\"WT.z_smev=Share\",\"WT.z_smShare=1\")";
	arr_ss [4][5] = "_new";
	
	// Stumbleupon
	arr_ss [5] = new Array()
	arr_ss [5][0] = "/en_GX/webadmin/assets/image/share_stumbleupon.gif";
	arr_ss [5][1] = "http://www.stumbleupon.com/submit?url=" + escape(window.location);
	arr_ss [5][2] = "?WT.mc_id=MRK091111SM5";
	arr_ss [5][3] = "Stumbleupon";
	arr_ss [5][4] = "wrs_trackclick(\"DCS.dcsuri= http://www.stumbleupon.com_sharebookmark\",\"WT.ti=Offsite: http://www.stumbleupon.com_sharebookmark\",\"WT.dl=24\",\"WT.z_smch=StumbleUpon\",\"WT.z_smev=Share\",\"WT.z_smShare=1\")";
	arr_ss [5][5] = "_new";
	
	// Delicious
	// arr_ss [6] = new Array()
	// arr_ss [6][0] = "/en_GX/webadmin/assets/image/share_delicious.gif";
	// arr_ss [6][1] = "http://del.icio.us/post?url=" + escape(window.location);
	// arr_ss [6][2] = "?WT.mc_id=MRK091111SM5";
	// arr_ss [6][3] = "Delicious";
	// arr_ss [6][4] = "wrs_trackclick(\"DCS.dcsuri=http://www.delicious.com_sharebookmark\",\"WT.ti=Offsite: http://www.delicious.com_sharebookmark\",\"WT.dl=24\")";
	// arr_ss [6][5] = "_new";
	

	
/* Header Logo */

// $(document).ready(function(){if($("#headerlogo").css('background-image')!='none') $("#headerlogo").wrap($(document.createElement("a")).attr('href',location.protocol+"//"+location.host+"/us/en/index.jhtml?WT.ac=pwc-logo")).css('cursor','pointer');});

/* Placeholder */