var questionData = {	
	Q1aSUM:{
		text: "What is your company's annual revenue?",
		category: ["Growth"],
		type: "",
		questions: []
	},
	Q1bSUM:{
		text: "What do you forecast your annual revenue will be in five years time",
		category: ["Growth"],
		type: "",
		questions: []
	},
	Q2:{
		text: "Which of the following phrases best describes your company's revenue growth ambitions over the next five years?",
		category: ["Growth"],
		type: "",
		questions: []
	},
	Q3a:{
		text: "What proportion of your revenue growth over the next five years will originate from organic growth, and what proportion will be inorganic growth?",
		flippedText: "What proportion of your revenue growth over the next five years will originate from *replace*?",
		category: ["Growth"],
		type: "StackedFaceted",
		questions: {"Q3a1":"organic growth","Q3a2":"inorganic growth"},
		order: [10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0],
		legendorder: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
	},
	Q4a:{
		text: "How important is innovation to the success of your company now and in five years time?",
		flippedText: "How important is innovation to the success of your company *replace*?",
		category: ["Appetite"],
		type: "StackedFaceted",
		questions: {"Q4a1":"now","Q4a2":"in five years time"},
		order: [0, 1, 2, 3, 4],
		legendorder: [4, 3, 2, 1, 0]
	},
	Q5:{
		text: "Which of the following statements best describes your company's appetite for innovation?",
		category: ["Appetite"],
		type: "",
		questions: []
	},
	Q6A:{
		text: "What percentage of your company's annual revenue is derived from major new products and services launched in the last year?",
		category: ["Appetite"],
		type: "",
		questions: []
	},
	Q6B:{
		text: "What percentage of your overall revenue is spent on innovation?",
		category: ["Appetite"],
		type: "",
		questions: []
	},
	Q7a:{
		text: "To what extent do you agree or disagree with the following statements about your innovation strategy?",
		flippedText: "",
		category: ["Strategy"],
		type: "StackedFaceted",
		questions: {"Q7a1":"Our company has a well-defined innovation strategy","Q7a2":"We have successfully implemented our innovation strategy thus far"},
		order: [0, 1, 2, 3, 4],
		legendorder: [4, 3, 2, 1, 0]
	},
	Q8:{
		text: " Which of the following is of greatest concern to you currently? ",
		category: ["Strategy"],
		type: "",
		questions: []
	},
	Q9Aa:{
		text: "How significant will your innovations in the following areas be over the next ... ",
		flippedText: "How significant will your innovations in the *replace* area be over the next ... ",
		category: ["Strategy"],
		type: "StackedFaceted",
		questions: {"Q9Aa1":"Products","Q9Aa2":"Services","Q9Aa3":"Technology","Q9Aa4":"Systems and processes","Q9Aa5":"Supply chain","Q9Aa6":"Customer experience","Q9Aa7":"Business model","Q9Aa8":"other"},
		order: [3, 2, 1, 0],
		legendorder: [0, 1, 2, 3]
	},
	Q9B:{
		text: "Of these areas, which is your priority for innovation the next 12 months? ",
		category: ["Strategy"],
		type: "",
		questions: []
	},
	Q10a:{
		text: "Does your company leverage social media to support innovation efforts in any of these areas?",
		flippedText: "Does your company leverage social media to support innovation efforts in *replace*?",
		category: ["Strategy"],
		type: "StackedFaceted",
		questions: {"Q10a1":"Open and unstructured forums","Q10a2":"Structured and disciplined processes for collecting and evaluating new ideas","Q10a3":"Conducting campaigns around specific problems to create innovative solutions","Q10a4":"Creating open innovation communities with people outside our organisation","Q10a5":"Other"},
		order: [0, 1],
		legendorder: [1, 0]
	},
	Q11Med1a:{
		text: "To what extent does your organisation leverage social, mobile, analytic and cloud technology to implement the following innovative strategies? ",
		flippedText: "To what extent does your organisation leverage social, mobile, analytic and cloud technology to implement the following innovative strategy: *replace*",
		category: ["Growth"],
		type: "StackedFaceted",
		questions: {"Q11Med1a1":"Engage consumers and patients in managing their health","Q11Med1a2":"Enable remote monitoring by gathering data from sensors","Q11Med1a3":"Integrate patient generated data into the clinical workflow","Q11Med1a4":"Integrate patient generated data to drive research outcomes","Q11Med1a5":"Integrate patient generated data to drive R&D of new technologies","Q11Med1a6":"Capture outcomes data to demonstrate the clinical effectiveness","Q11Med1a7":"Capture outcomes data to demonstrate the economic efficiency"},
		order: [2, 1, 0],
		legendorder: [0, 1, 2]
	},
/*	Q11Med2a:{
		text: "To what extent has your organisation leveraged social, mobile, analytic and cloud technologies to create any of the following new business models for products and services?",
		flippedText: "To what extent has your organisation leveraged social, mobile, analytic and cloud technologies to create any of the following new business models for products and services? *replace*",
		category: ["Growth"],
		type: "StackedFaceted",
		questions: {"Q11Med2a1":"New services that will generate","Q11Med2a2":"Comprehensive service packages","Q11Med2a3":"Comprehensive packages that","Q11Med2a4":"Comprehensive packages for which"},
		order: [2, 1, 0],
		legendorder: [0, 1, 2]
	}, 
*/	Q11Med3a:{
		text: "Does your company leverage social, mobile, analytic and cloud technologies in any of the following ways?",
		flippedText: "Does your company leverage social, mobile, analytic and cloud technologies to *replace*?",
		category: ["Growth"],
		type: "StackedFaceted",
		questions: {"Q11Med3a1":"To create a personal relationship with the patient","Q11Med3a2":"Create a better relationship with the physician and clinician","Q11Med3a3":"Differentiate from competitors in the minds of the customer procurement department","Q11Med3a4":"Justify reimbursement levels and value created in the minds of payers for our devices"},
		order: [0, 1],
		legendorder: [1, 0]
	},
	Q11a:{
		text: "Which of the following types of business model innovation will be you implementing over the next three years?",
		flippedText: "Which of the following types of business model innovation will be you implementing over the next three years? - *replace*",
		category: ["Strategy"],
		type: "StackedFaceted",
		questions: {"Q11a1":"New value offerings","Q11a2":"Lower-cost models","Q11a3":"Enhancing the customer experience","Q11a4":"Finding new ways to monetize existing products / services","Q11a5":"Servicing un-served or under-served customers","Q11a6":"Other"},
		order: [0, 1],
		legendorder: [1, 0]
	},
	Q12A:{
		text: "Which of the following best describes the way that your company manages its innovation processes? ",
		category: ["Execution"],
		type: "",
		questions: []
	},
	Q12Ba:{
		text: "Which of the following approaches to managing innovation does your organisation take?",
		flippedText: "Which of the following approaches to managing innovation does your organisation take? *replace*",
		category: ["Execution"],
		type: "StackedFaceted",
		questions: {"Q12Ba1":"Individual product areas or services are responsible for their own innovations","Q12Ba2":"We have a formal innovation structures within individual business units","Q12Ba3":"We have separate innovation facilities in important markets","Q12Ba4":"We drive innovation across the entire organisation, across business units and territories"},
		order: [0, 1],
		legendorder: [1, 0]
	},
	Q13:{
		text: "Which of these approaches do you think will lead to innovations that drive the most growth for your company? ",
		category: ["Execution"],
		type: "",
		questions: []
	},
	Q14A:{
		text: "What percentage of your innovative products and services are co-created with customers? By this we mean customers are involved from idea generation to execution.",
		category: ["Collaboration"],
		type: "",
		questions: []
	},
	Q14B:{
		text: "What percentage of your innovative products and services are developed jointly with external partners? By this we mean external partners are involved from idea generation to execution.",
		category: ["Collaboration"],
		type: "",
		questions: []
	},
	Q15a:{
		text: "With which of the following do you have a plan in place to collaborate over the next three years to deliver innovative products and services?",
		flippedText: "With which of the following do you have a plan in place to collaborate over the next three years to deliver innovative products and services? *replace*",
		category: ["Collaboration"],
		type: "StackedFaceted",
		questions: {"Q15a1":"Customers","Q15a2":"Competitors","Q15a3":"Strategic partners","Q15a4":"Suppliers","Q15a5":"Academics","Q15a6":"Other"},
		order: [0, 1],
		legendorder: [1, 0]
	},
	Q16Aa:{ 
		text: "Does your organisation take advantage of any of the following to support your innovations?",
		flippedText: "Does your organisation take advantage of any of the following to support your innovations? *replace*",
		category: ["Collaboration"],
		type: "StackedFaceted",
		questions: { "Q16Aa1":"Government funding", "Q16Aa2":"Tax incentives", "Q16Aa3":"Other innovation incentivisation schemes" },
		order: [0, 1],
		legendorder: [1, 0]
	},
	Q16Ba:{
		text: "To what extent do you agree or disagree with the following statements about government’s innovation policy in your home market?",
		flippedText: "To what extent do you agree or disagree with the following statements about government’s innovation policy in your home market? *replace*",
		category: ["Collaboration"],
		type: "StackedFaceted",
		questions: {"Q16Ba1":"Laws and regulations around contracts and protection of intellectual property ", "Q16Ba2":"Government assistance is helpful in boosting our innovation output", "Q16Ba3":"General tax relief or lower corporate tax rates is more helpful than incentives"},
		order: [0, 1, 2, 3, 4],
		legendorder: [4, 3, 2, 1, 0]
	},
	Q17a:{
		text: "How challenging do you find the following aspects of making innovation happen within your company?",
		flippedText: "How challenging do you find the following aspects of making innovation happen within your company? *replace*",
		category: ["Challenges"],
		type: "StackedFaceted",
		questions: {"Q17a1":"Establishing an innovative culture internally", "Q17a2":"Taking innovative ideas to market quickly and in a scalable way", "Q17a3":"Having the right metrics to measure innovation progress and track ROI (return on investment)", "Q17a4":"Finding and retaining the best talent to make innovation happen", "Q17a5":"Finding the right external partners to collaborate with" },
		order: [0, 1, 2, 3, 4],
		legendorder: [4, 3, 2, 1, 0]
	},
	Q19a:{
		text: "In your organisation, how important are each of the following to creating and fostering an innovative culture?",
		flippedText: "In your organisation, how important are each of the following to creating and fostering an innovative culture? *replace*",
		category: ["Challenges"],
		type: "StackedFaceted",
		questions: {"Q19a1":"Recognising and rewarding innovation initiatives", "Q19a2":"Senior executives participating in innovation projects", "Q19a3":"Setting up internal communities of interest", "Q19a4":"Offering employees opportunity to lead or participate in high-profile innovation initiatives", "Q19a5":"Fostering an environment where failure and risk are reasonably tolerated", "Q19a6":"Giving the innovation function equal status to other functional areas", "Q19a7":"Having well-defined and accepted processes for innovation"},
		order: [0, 1, 2, 3, 4],
		legendorder: [4, 3, 2, 1, 0]
	}
	
};

	/*QC:{
		text: "Please select country ",
		category: [],
		type: "",
		questions: []
	},
	QS1:{
		text: "Before we begin, can you confirm your job title? ",
		category: [],
		type: "",
		questions: []
	},
	QS2:{
		text: "Do you have responsibility for innovation within your company? By innovation we mean new approaches to products and services, business models, or customer experience.",
		category: [],
		type: "",
		questions: []
	},
	QS3:{
		text: "Are you answering the following questions on behalf of your organisation as a whole, or a specific division, region or country?",
		category: [],
		type: "",
		questions: []
	},
	QS4:{
		text: "Finally, can you confirm which sector your organisation primarily falls into?",
		category: [],
		type: "",
		questions: []
	},
	QS4h:{
		text: "Sector",
		category: [],
		type: "",
		questions: []
	},
	Q20:{
		text: "How many full time staff does your organisation currently employ?",
		category: [],
		type: "",
		questions: []
	},
	Q21:{
		text: "Please confirm your company type. Is it: ",
		category: [],
		type: "",
		questions: []
	},
	Q22:{
		text: "Would you be willing to take part in a follow up interview to explore some of these issues in more detail? ",
		category: [],
		type: "",
		questions: []
	},
	Q23:{
		text: "Would you like a copy of PwC's research report when this study is completed? ",
		category: [],
		type: "",
		questions: []
	},
	Q24:{
		text: "Are you happy for your responses to this questionnaire to be passed directly back to PwC or would you prefer them to be kept anonymous, in aggregate form only?",
		category: [],
		type: "",
		questions: []
	},
	Q25:{
		text: "Are you happy for your company to be cited in PwC's innovation report as a contributor to this study?",
		category: [],
		type: "",
		questions: []
	}
	*/


