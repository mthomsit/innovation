var REGISTRATION_ID = "registration_id";
var registrationId = null;
$(document).ready(function() {
    registrationId =  $.cookie(REGISTRATION_ID);
    $('.download-trigger').hide();
    $('.download-register').hide();

    $('.icon,.file-name').click(function(e) {
        e.stopPropagation();
        e.preventDefault();
        
        var download = $(this).parents('.download');
        var downloadId = download.attr('id').replace('download','');
        
        if (registrationId) {
            // user has already registered
            $('#trigger' + downloadId + ' input[type="submit"]').click();
        }
        else {
            // user needs to register - show the download form
            var form = $('#register' + downloadId);
            form.fadeIn(300);
        }
    });
  
  if (registrationId) {
    // add the registration id to all downloads
    var html = '<input type="hidden" value="' + registrationId + '" name="person[id]">';
    $('.download-trigger').append(html);
  }
  else {    
    $('.download-register input[type="submit"]').click(function(e) {
      e.stopPropagation();
      e.preventDefault();

      // get the post endpoint
      var that = $(this);
      var form = that.parents('form');
      var url = form.attr('action');
      
      if (
        ($('input.firstname',form).val() != '') &&
        ($('input.lastname',form).val() != '') &&
        ($('input.email',form).val() != '') &&
        ($('input.country',form).val() != '')
      ) {
        // continue
      }
      else {
        alert('Please complete the details shown above');
        return;
      }

      var xhr = $.ajax({
        type:'GET',
        url: url,
        data: form.serialize(),
        dataType: 'jsonp',
        success: function(data) {
            if (data) {
                var personId = data['id'];
                var campaignId = data['campaign_id'];
                var downloadId = data['download_id'];

                // store the cookie for another download
                registrationId = personId;
                $.cookie(REGISTRATION_ID, registrationId);

                // hide all the register forms
                $('.download-register').hide();

                // set the registration id into all download forms and show them
                var html = '<input type="hidden" value="' + registrationId + '" name="person[id]">';
                $('.download-trigger').append(html);
                $('.download-trigger').show();

                // trigger the download of the file the user registered for
                $('#trigger' + downloadId + ' input[type="submit"]').click();
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            //
            alert('Error: ' + textStatus + ', ' + errorThrown);
        }
      });
     
    });
    
  }

  $('#clear-cookie').click(function(e) {
    e.preventDefault();
    e.stopPropagation();

    $.cookie(REGISTRATION_ID, '');
  });
});
