$(document).ready(function() {

    $('#main')
        .css('overflow','hidden')
        .css('height','500px');
    $('#main-inner')
        .css('position', 'relative')
        .css('top', '0px')

    $('.primary a').click(function(e) {
        e.preventDefault();
        e.stopPropagation();

        var attributes = {};
        if ($(this).parent().attr('id') == 'road-to-growth-nav') {
            attributes = {'top': '0px'}
        }
        else if ($(this).parent().attr('id') == 'explore-the-data-nav') {
            attributes = {'top': '-500px'}
        }
        /*else if ($(this).parent().attr('id') == 'benchmark-your-organisation-nav') {
            attributes = {'top': '-1000px'}
        }*/
        else if ($(this).parent().attr('id') == 'about-the-survey-nav') {
            attributes = {'top': '-1000px'}
        }
        else if ($(this).parent().attr('id') == 'downloads-nav') {
            attributes = {'top': '-1500px'}
        }
        $('#main-inner').stop().animate(attributes,300);

        var href = $(this).attr('href');
        var o = $(href);
        if (o) {
            var top = $(o).attr('data-offset-top');
            if (top) {
                var floatTop = parseFloat(top);
                var offsetTop = floatTop - offsets[ROAD_TO_GROWTH];

                $('#main-inner').animate({top : '-' + offsetTop + 'px'}, 500);//;css('top', '-' + offsetTop + 'px');
            }

            // section behaviour
            $('.section').removeClass('active');
            o.addClass('active');
        }

        // tab behaviour
        $('.primary li').removeClass('active');
        $(this).parent().addClass('active');
    });
});