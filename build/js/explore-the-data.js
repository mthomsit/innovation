$(document).ready(function() {
    var html = [];
    html[html.length]  ='<select name="question" id="question">';
    html[html.length] = '<option value="">Select a question ...</option>';
    
    var previous = '';
    var current = '';
    
    var first = true;
    
    for (var key in questionData) {
        var question = questionData[key];
        
        var number = key;
        var categories = question.category;
        var title = question.text;
        
        var category = '';
        if (categories.length > 0) {
            category = categories[0];
        }
        
        if (category != '') {
            previous = current;
            current = category;
            
            if (previous != current) {
                if (!first) {
                    html[html.length] = '</optgroup>';
                }
                html[html.length] = '<optgroup label="' + current + '">'
                first = false;
            }
            
            html[html.length] = '<option value="' + number + '">' + title + '</option>';
        }
    }
    
    html[html.length] = '</select>'
    $('#explore-the-data-form #industry').before(html.join("\n"));
    
    //
    window.setTimeout(function() {
        var select = $('#explore-the-data-form select');
        select.selectbox().change(function() {
            var val = $(this).val();
            if (val) {
                //var url = '/gx/en/innovationsurvey/data-explorer.jhtml?q=' + val + '&hideInterface=1&width=546&height=360';
                //$('#explore-frame').attr('src', url);
            }
        });
    },10);

    var change = $('#explore-the-data-form input[type="submit"]');
    change.click(function(e) {
        e.preventDefault();
        e.stopPropagation();

        var question = $('#question').val();
        var industry = $('#industry').val();
        var geography = $('#geography').val();

        var url = 'http://www.pwc.com/gx/en/innovationsurvey/data-explorer.jhtml?q=' + question + '&hideInterface=1&width=546&height=360';

        /*$('#explore-frame').load(function() {
            alert('frame loaded');
        }); */

        $('#explore-frame').attr('src', url);



    })
});