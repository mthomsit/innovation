// make it safe to use console.log always
(function(b){function c(){}for(var d="assert,count,debug,dir,dirxml,error,exception,group,groupCollapsed,groupEnd,info,log,timeStamp,profile,profileEnd,time,timeEnd,trace,warn".split(","),a;a=d.pop();){b[a]=b[a]||c}})((function(){try
{console.log();return window.console;}catch(err){return window.console={};}})());

// remove deprecated layerX and layerY from event list, prevent errors
$.event.props = $.event.props.join('|').replace('layerX|layerY|', '').split('|');

/*jshint multistr:true */


var hasTouch = 'ontouchstart' in window,
	START_EV = hasTouch ? 'touchstart' : 'mousedown',
	MOVE_EV = hasTouch ? 'touchmove' : 'mousemove',
	END_EV = hasTouch ? 'touchend' : 'mouseup',
	CANCEL_EV = hasTouch ? 'touchcancel' : 'mouseup';


var kPWCRedFillColor = '#a32020',
	kPWCDarkRedFillColor = '#80021a',
	kPWCLightRedFillColor = '#d7b1a9',
	kPWCOrangeWellColor = '#e8dfcf',
	kPWCOrangeFillColor = '#eb8c00',
	kPWCPinkFillColor = '#ef4062',
	kPWCDarkPinkFillColor = '#b82234',
	kPWCLightPinkFillColor = '#fbcfcc';

var kRoadMapLeftMargin = 60,
	kRoadMapStepWidth = 180,
	kRoadMapStepGraphicHeight = 160,
	kRoadMapStepMargin = 30,
	kRoadMapStepIndicatorWidth = 20;

var kPWCisMobile = function() {
	return $('body').attr('id') == 'pwcmobile';
};

/* __________________________________________________________________________________________ data model
*/

var DataModel = function() {
	var that = this,
		data,
		onerror,
		onload;
	return that;
};

DataModel.prototype = {
	init: function() {
		var that = this;
		$.ajax({
			type: "GET",
			url: "assets/data.json",
			dataType: "json",
			success: function(json_) {
				that.data = json_;
				that.onload();
			},
			error: function(jqXHR, textStatus, errorThrown) {
				that.onerror();
			}
		});
		return that;
	},

	onload:function(onload_) {
		var that = this;
		that.onload = onload_;
		return that;
	},

	onerror:function(onerror_) {
		var that = this;
		that.onerror = onerror_;
		return that;
	}
};

/* __________________________________________________________________________________________ road map view
*/

var RoadMapView = function() {
	var that = this,
		container,
		currentStep,
		dataModel,
		roadMapStepViews,
		roadWell,
		roadFill,
		currentStepIndicator,
		currentStepIndicatorWell,
		currentStepIndicatorFill;
	return that;
};

RoadMapView.prototype = {
	init: function(dataModel_) {
		var that = this,
			roadMapStepView,
			stepIndication,
			i;
		that.dataModel = dataModel_;
		that.roadMapStepViews = [];
		that.container = $('#road-map');
		that.currentStep = 0;

		that.roadWell = $('<div class="road-well" />');
		that.roadFill = $('<div class="road-fill" />');
		that.container.append(that.roadWell);
		that.container.append(that.roadFill);

		that.currentStepIndicator = $('<div class="step-indicator" />');
		that.container.parent().append(that.currentStepIndicator);
		that.currentStepIndicatorWell = $('<div class="road-well" />');
		that.currentStepIndicatorFill = $('<div class="road-fill" />');
		that.currentStepIndicator.append(that.currentStepIndicatorWell);
		that.currentStepIndicator.append(that.currentStepIndicatorFill);

		for ( i = 0; i < that.dataModel.data.length; i++ ) {
			roadMapStepView = new RoadMapStepView().init(that.dataModel.data[i],that,i);
			that.container.append(roadMapStepView.container);
			that.roadMapStepViews.push(roadMapStepView);
			if ( i === that.dataModel.data.length-1 ) {
				roadMapStepView.setNextHidden(true);
			}
			stepIndication = $('<div data-step="'+i+'" />');
			$(stepIndication).click(function(e){
				that.setCurrentStep(parseInt($(this).attr('data-step')), true);
			});
			$(stepIndication).css({
				"left":i*kRoadMapStepIndicatorWidth+"px"
			});
			that.currentStepIndicator.append(stepIndication);
		}
		that.layout();
		that.setCurrentStep(0, true);
		containerWidth = $(that.container).parent().innerWidth();
		$(that.container).css({
			"left":containerWidth+"px"
		});
		$(that.container).delay(500).animate({
			"left":"0px"
		},750);

		$(window).keydown(function(event) {
			switch (event.keyCode) {
				case 37:
					that.previous();
					break;
				case 39:
					that.next();
					break;
			}
		});
		return that;
	},

	layout:function() {
		var that = this,
			roadMapStepView,
			containerWidth,
			currentStepIndicatorContainerWidth,
			i;
		containerWidth = $(that.container).parent().innerWidth();
		currentStepIndicatorContainerWidth = kRoadMapStepIndicatorWidth * that.dataModel.data.length;
		for ( i = 0; i < that.roadMapStepViews.length; i++ ) {
			roadMapStepView = that.roadMapStepViews[i];
			roadMapStepView.container.css({
				"left":(kRoadMapLeftMargin+(kRoadMapStepWidth+kRoadMapStepMargin)*i)+"px",
				"width":(kRoadMapStepWidth+kRoadMapStepMargin)+"px"
			});
		}
		$(that.container).css({
			"width":(kRoadMapLeftMargin+(kRoadMapStepWidth+kRoadMapStepMargin)*that.roadMapStepViews.length-kRoadMapStepMargin)+"px"
		});
		$(that.roadWell).css({
			"left":(kRoadMapLeftMargin+14)+"px",
			"width":(kRoadMapLeftMargin+(kRoadMapStepWidth+kRoadMapStepMargin)*(that.roadMapStepViews.length-1)-kRoadMapStepMargin-28)+"px"
		});
		$(that.roadFill).css({
			"left":(kRoadMapLeftMargin+14)+"px",
			"width":(kRoadMapLeftMargin+(kRoadMapStepWidth+kRoadMapStepMargin)*(that.currentStep)-kRoadMapStepMargin-28)+"px"
		});
		that.currentStepIndicator.css({
			"left":Math.round((containerWidth-currentStepIndicatorContainerWidth)*0.5)+"px",
			"width":currentStepIndicatorContainerWidth+"px"
		});
		$(that.currentStepIndicatorWell).css({
			"left":(0.5*kRoadMapStepIndicatorWidth)+"px",
			"width":(currentStepIndicatorContainerWidth-kRoadMapStepIndicatorWidth)+"px"
		});
		$(that.currentStepIndicatorFill).css({
			"left":(0.5*kRoadMapStepIndicatorWidth)+"px",
			"width":(kRoadMapStepIndicatorWidth*that.currentStep)+"px"
		});
		return that;
	},

	viewTheData:function(id_,url) {
		//alert('viewTheData:'+id_);
		location.href=url;
	},

	next:function() {
		var that = this;
		that.setCurrentStep(that.currentStep+1, true);
		return that;
	},

	previous:function() {
		var that = this;
		that.setCurrentStep(that.currentStep-1, true);
		return that;
	},

	setCurrentStep:function(currentStep_,animated_) {
		var that = this,
			i,
			roadMapStepView,
			rightOfCurrentStep,
			centerOfCurrentStep,
			currentLeft,
			newLeft,
			maxLeft,
			containerWidth,
			attributes;

		// sanitise
		while (currentStep_ < 0 ) {
			currentStep_ += that.dataModel.data.length
		}
		that.currentStep = currentStep_ % that.dataModel.data.length;
		centerOfCurrentStep = (kRoadMapStepWidth+kRoadMapStepMargin)*(that.currentStep) + 0.5 * kRoadMapStepWidth;
		currentLeft = $(that.container).attr('left') ? parseInt($(that.container).attr('left')) : 0;
		containerWidth = $(that.container).parent().innerWidth() - kRoadMapLeftMargin;
		newLeft = Math.round(-centerOfCurrentStep + 0.5 * containerWidth);
		newLeft = Math.min(0,newLeft);
		maxLeft = (kRoadMapStepWidth+kRoadMapStepMargin)*that.dataModel.data.length - containerWidth;
		newLeft = Math.max(-maxLeft,newLeft);
		attributes = {
			"left":newLeft
		};
		if ( animated_ ) {
			$(that.container).stop().animate(attributes,300);
		}
		else {
			$(that.container).css(attributes);
		}
		attributes = {
			"left":(kRoadMapLeftMargin+14)+"px",
			"width":(kRoadMapLeftMargin+(kRoadMapStepWidth+kRoadMapStepMargin)*(that.currentStep)-kRoadMapStepMargin-28)+"px"
		};
		if ( animated_ ) {
			$(that.roadFill).stop().animate(attributes,300);
		}
		else {
			$(that.roadFill).css(attributes);
		}
		attributes = {
			"left":(0.5*kRoadMapStepIndicatorWidth)+"px",
			"width":(kRoadMapStepIndicatorWidth*that.currentStep)+"px"
		};
		if ( animated_ ) {
			$(that.currentStepIndicatorFill).stop().animate(attributes,300);
		}
		else {
			$(that.currentStepIndicatorFill).css(attributes);
		}
		for ( i = 0; i < that.roadMapStepViews.length; i++ ) {
			roadMapStepView = that.roadMapStepViews[i];
			if ( i <= that.currentStep ) {
				roadMapStepView.setHighlighted(true, animated_);
				// roadMapStepView.setExpanded(true, animated_);
				$('[data-step="'+i+'"]',that.currentStepIndicator).addClass('highlighted');
			}
			else {
				roadMapStepView.setHighlighted(false, animated_);
				// roadMapStepView.setExpanded(false, animated_);
				$('[data-step="'+i+'"]',that.currentStepIndicator).removeClass('highlighted');
			}
			if ( i == that.currentStep ) {
				roadMapStepView.setExpanded(true, animated_);
			}
			else {
				roadMapStepView.setExpanded(false, animated_);
			}
		}
		return that;
	}
};

/* __________________________________________________________________________________________ road map step view
*/

var RoadMapStepView = function() {
	var that = this,
		container,
		roadMapView,
		highlighted,
		expanded,
		paper,
		topBuildingView,
		bottomBuildingView,
		stepNumber,
		dataModel;
	return that;
};

RoadMapStepView.prototype = {
	init: function(dataModel_,roadMapView_,stepNumber_) {
		var that = this;
		that.dataModel = dataModel_;
		that.roadMapView = roadMapView_;
		that.stepNumber = stepNumber_;
		that.container = $('\
			<div class="step-view" data-step="'+that.stepNumber+'">\
				<div class="step-view-graphic">\
				</div>\
				<div class="road-indicator" >\
				</div>\
				<div class="annotation">\
					<div class="teaser">'+that.dataModel.teaser+'</div>\
					<div class="body">\
						'+that.dataModel.body+'\
						<div class="actions">\
							<a href="#" class="button primary">View the data</a>\
							<a href="#" rel="next" class="button" data-step="'+that.stepNumber+'" >Next step</a>\
						</div>\
					</div>\
				</div>\
			</div>\
		');
		$('.teaser',that.container).click(function(e) {
			e.preventDefault();
			that.roadMapView.setCurrentStep(parseInt($(this).attr('data-step')), true);
		});
		$('.road-indicator',that.container).click(function(e) {
			e.preventDefault();
			that.roadMapView.setCurrentStep(parseInt($(this).attr('data-step')), true);
		});
		$('a.primary',that.container).click(function(e) {
			e.preventDefault();
			that.roadMapView.viewTheData(that.dataModel.id, that.dataModel.url);
		});
		$('a[rel="next"]',that.container).click(function(e) {
			e.preventDefault();
			e.stopImmediatePropagation();
			that.roadMapView.setCurrentStep(1+parseInt($(this).attr('data-step')), true);
		});
		that.paper = Raphael($('.step-view-graphic',that.container).get(0), kRoadMapStepWidth, kRoadMapStepGraphicHeight);
		that.bottomBuildingView = new BuildingView().init(that.paper,false);
		that.topBuildingView = new BuildingView().init(that.paper,true);
		that.bottomBuildingView.labelOverride = that.dataModel.bottomLabelOverride;
		that.topBuildingView.labelOverride = that.dataModel.topLabelOverride;
		return that;
	},

	setNextHidden:function(hidden_) {
		var that = this;
		if ( hidden_ ) {
			$('a[rel="next"]',that.container).text('Start again');
		}
		else {
			$('a[rel="next"]',that.container).text('Next step');
		}
		return that;
	},

	setHighlighted:function(highlighted_, animated_) {
		var that = this,
			attributes;
		if ( highlighted_ === that.highlighted ) {
			return;
		}
		that.highlighted = highlighted_;
		if ( that.highlighted ) {
			$(that.container).addClass('highlighted');
		}
		else {
			$(that.container).removeClass('highlighted');
		}
		if ( animated_ ) {
			that.topBuildingView.setValueAnimated(that.highlighted ? that.dataModel.top/100 : 0);
			that.bottomBuildingView.setValueAnimated(that.highlighted ? that.dataModel.bottom/100 : 0);
		}
		else {
			that.topBuildingView.setValue(that.highlighted ? that.dataModel.top/100 : 0);
			that.bottomBuildingView.setValue(that.highlighted ? that.dataModel.bottom/100 : 0);
		}
		if ( animated_ ) {
			if ( that.highlighted ) {
				$('.body',that.container).stop(true, true).fadeIn(300);
			}
			else {
				$('.body',that.container).stop(true, true).fadeOut(300);
			}
		}
		else {
			if ( that.highlighted ) {
				$('.body',that.container).stop(true, true).fadeIn(0);
			}
			else {
				$('.body',that.container).stop(true, true).fadeOut(0);
			}
		}
		return that;
	},

	setExpanded:function(expanded_, animated_) {
		var that = this;
		if ( expanded_ === that.expanded ) {
			return;
		}
		that.expanded = expanded_;
		if ( animated_ ) {
			if ( that.expanded ) {
				$('.actions',that.container).stop(true, true).fadeIn(300);
			}
			else {
				$('.actions',that.container).stop(true, true).fadeOut(300);
			}
		}
		else {
			if ( that.expanded ) {
				$('.actions',that.container).stop(true, true).fadeIn(0);
			}
			else {
				$('.actions',that.container).stop(true, true).fadeOut(0);
			}
		}
		return that;
	}
};

/* __________________________________________________________________________________________ building view
*/

var BuildingView = function() {
	var that = this,
		paper,
		elementShadow,
		elementFront,
		elementLeft,
		elementTop,
		indicatorText,
		value,
		labelOverride,
		offsetLeft,
		isTopStyle;
	return that;
};

var kBuildingViewHeightMax = 100,
	kBuildingViewHeightMin = 5,
	kBuildingViewTopMargin = 20,
	kBuildingSideWidth = 10,
	kBuildingRoofHeight = 16,
	kBuildingWidth = 30,
	kBuildingTopStyleOffset = 60;
	kBuildingBottomStyleOffset = 120;

BuildingView.prototype = {
	init: function(paper_,isTopStyle_) {
		var that = this,
			left;

		that.paper = paper_;
		that.isTopStyle = isTopStyle_;
		that.offsetLeft = that.isTopStyle ? kBuildingTopStyleOffset : kBuildingBottomStyleOffset;
		that.value = 0;

		that.elementShadow = that.paper.path(that.getElementShadowPath()).attr({
			"fill": '#a3968b',
			"stroke": "none"
		});
		that.elementFront = that.paper.path(that.getElementFrontPath()).attr({
			"fill": that.isTopStyle ? kPWCPinkFillColor : kPWCRedFillColor,
			// "fill": "url('img/building-front.png')",
			"stroke": "none"
		});
		that.elementLeft = that.paper.path(that.getElementLeftPath()).attr({
			"fill": that.isTopStyle ? kPWCDarkPinkFillColor : kPWCDarkRedFillColor,
			"stroke": "none"
		});
		that.elementTop = that.paper.path(that.getElementTopPath()).attr({
			"fill": that.isTopStyle ? kPWCLightPinkFillColor : kPWCLightRedFillColor,
			"stroke": "none"
		});

		left = kBuildingSideWidth + that.offsetLeft;

		that.indicatorText = that.paper.text(left+kBuildingWidth/2, 10, "").attr({
			"font-size": 16,
			"font-family": "Georgia, serif",
			"font-weight": "bold",
			"fill": that.isTopStyle ? kPWCPinkFillColor : kPWCRedFillColor
		});

		that.setValue(0);

		return that;
	},

	setValueAnimated:function(value_) {
		var that = this;
		// that.value = value_;
		// that.elementFront.animate({
		// 	path:that.getElementFrontPath()
		// },300);
		TweenLite.to(that, 0.3, {
			setValue:value_,
			ease:Quad.easeOut
		});
		return that;
	},

	getElementFrontPath:function() {
		var that = this,
			path = '',
			left = kBuildingSideWidth + that.offsetLeft,
			height = Math.round(kBuildingViewHeightMin+(kBuildingViewHeightMax-kBuildingViewHeightMin)*that.value),
			bottom = kBuildingViewTopMargin+kBuildingRoofHeight+kBuildingViewHeightMax;

		// move to bottom left corner
		path += 'M'+left+' '+bottom;

		// horizontal line to bottom right
		path += 'h'+kBuildingWidth;

		// vertical line to top right
		path += 'v-'+height;

		// horizontal line to top left
		path += 'h-'+kBuildingWidth;

		return path;
	},

	getElementLeftPath:function() {
		var that = this,
			path = '',
			left = that.offsetLeft,
			height = Math.round(kBuildingViewHeightMin+(kBuildingViewHeightMax-kBuildingViewHeightMin)*that.value),
			bottom = kBuildingViewTopMargin+kBuildingViewHeightMax;

		path += 'M'+left+' '+bottom;
		path += 'l'+kBuildingSideWidth+' '+kBuildingRoofHeight;
		path += 'v-'+height;
		path += 'l-'+kBuildingSideWidth+' -'+kBuildingRoofHeight;

		return path;
	},

	getElementTopPath:function() {
		var that = this,
			path = '',
			left = that.offsetLeft,
			height = Math.round(kBuildingViewHeightMin+(kBuildingViewHeightMax-kBuildingViewHeightMin)*that.value),
			bottom = kBuildingViewTopMargin+kBuildingViewHeightMax;

		path += 'M'+left+' '+(bottom-height);
		path += 'l'+kBuildingSideWidth+' '+kBuildingRoofHeight;
		path += 'h'+kBuildingWidth;
		path += 'l-'+kBuildingSideWidth+' -'+kBuildingRoofHeight;

		return path;
	},

	getElementShadowPath:function() {
		var that = this,
			path = '',
			left = that.offsetLeft,
			height = Math.round(kBuildingViewHeightMin+(kBuildingViewHeightMax-kBuildingViewHeightMin)*that.value),
			bottom = kBuildingViewTopMargin+kBuildingViewHeightMax;

		m = (kBuildingViewHeightMin + (kBuildingViewHeightMax-kBuildingViewHeightMin)*that.value) / kBuildingViewHeightMax;
		vx = m * 60;
		vy = m * 20;
		path += 'M'+left+' '+bottom;
		path += 'l'+kBuildingSideWidth+' '+kBuildingRoofHeight;
		path += 'l-'+vx+' -'+vy;
		path += 'l-'+kBuildingSideWidth+' -'+kBuildingRoofHeight;
		path += 'h'+kBuildingWidth;
		path += 'l'+vx+' '+vy;
		return path;
	},

	setValue:function(value_) {
		var that = this,
			height = Math.round(kBuildingViewHeightMin+(kBuildingViewHeightMax-kBuildingViewHeightMin)*value_),
			bottom = kBuildingViewTopMargin+kBuildingRoofHeight+kBuildingViewHeightMax;

		that.value = value_;

		p = (that.value - that.minValue) / (that.maxValue - that.minValue);
		p = Math.round(p*100);

		that.elementShadow.attr({
			path:that.getElementShadowPath()
		});
		that.elementFront.attr({
			path:that.getElementFrontPath()
		});
		that.elementLeft.attr({
			path:that.getElementLeftPath()
		});
		that.elementTop.attr({
			path:that.getElementTopPath()
		});
		if ( that.value === 0 ) {
			that.indicatorText.attr({
				text: " "
			});
		}
		else {
			that.indicatorText.attr({
				text: that.labelOverride ? that.labelOverride : Math.round(100*that.value)+"%"
			});
		}
		that.indicatorText.attr({
			y:bottom - height - 30
		});
		return that;
	},

	getValue:function() {
		var that = this;
		return that.value;
	}
};

/* __________________________________________________________________________________________ startup
*/


$(window).load(function() {
	var dataModel,
		roadMapView;

	dataModel = new DataModel().onload(function() {
		roadMapView = new RoadMapView().init(dataModel);
	}).init();
});

