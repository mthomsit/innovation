$(document).ready(function() {

    $('.icon,.file-name').click(function(e) {
        e.stopPropagation();
        e.preventDefault();
        
        var download = $(this).parents('.download');
        var id = download.attr('id').replace('download-','') + '-contact-us.jhtml';
        
        var url = '/gx/en/innovationsurvey/' + id;
        location.href = url;
    });

});
